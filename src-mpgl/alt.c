/*
Note: some of the early MicroProse games with a similar
  structure (Knights of the Sky, for instance) seem to
  have EXEs compressed using LZ91.
*/

struct fileHeader {
    char name[12];
    unsigned short u1;  // maybe related to read state
    unsigned int size;
    unsigned int offset;
}

//////

// NOTE: many of the routines (these are from early TTD source) are not included!

static void GfxMainBlitter(byte *sprite, int x, int y, int mode);
void GfxInitPalettes();

static void memcpy_pitch(void *d, void *s, int w, int h, int spitch, int dpitch)
{
	byte *dp = (byte*)d;
	byte *sp = (byte*)s;

	assert(h >= 0);
	if (h != 0) do {
		memcpy(dp, sp, w);
		dp += dpitch;
		sp += spitch;
	} while (--h);
}

void GfxFillRect(int left, int top, int right, int bottom, int color) {
	DrawPixelInfo *dpi = _cur_dpi;
	byte *dst,*ctab;

	if (dpi->zoom != 0)
		return;

	if (left > right || top > bottom)
		return;

	if (right < dpi->left || left >= dpi->left + dpi->width)
		return;

	if (bottom < dpi->top || top >= dpi->top + dpi->height)
		return;

	if ( (left -= dpi->left) < 0) left = 0;
	right = right - dpi->left + 1;
	if (right > dpi->width) right=dpi->width;
	right -= left;
	assert(right > 0);

	if ( (top -= dpi->top) < 0) top = 0;
	bottom = bottom - dpi->top + 1;
	if (bottom > dpi->height) bottom=dpi->height;
	bottom -= top;
	assert(bottom > 0);

	dst = dpi->dst_ptr + top * dpi->pitch + left;

	if (!(color & 0x8000)) {
		if (!(color & 0x4000)) {
			do {
				memset(dst, color, right);
				dst += dpi->pitch;
			} while (--bottom);
		} else {
			/* use colortable mode */
			ctab = GetSpritePtr(color & 0x3FFF) + 1;
			do {
				int i;
				for(i=0; i!=right;i++)
					dst[i] = ctab[dst[i]];
				dst += dpi->pitch;
			} while (--bottom);
		}
	} else {
		byte bo = 0;
		do {
			int i;
			byte b = (bo^=1);
			for(i=0; i!=right;i++)
				if ((b^=1) != 0)
					dst[i] = (byte)color;
			dst += dpi->pitch;
		} while (--bottom);
	}
}

void GfxSetPixel(int x, int y, int color)
{
	DrawPixelInfo *dpi = _cur_dpi;
	if ((x-=dpi->left) < 0 || x>=dpi->width || (y-=dpi->top)<0 || y>=dpi->height)
		return;
	dpi->dst_ptr[y*dpi->pitch+x] = color;
}

void GfxDrawLine(int x, int y, int x2, int y2, int color)
{
	int dy;
	int dx;
	int stepx, stepy;
	int frac;

	// Check clipping first
	{
		DrawPixelInfo *dpi = _cur_dpi;
		int t;

		if (x < dpi->left && x2 < dpi->left)
			return;

		if (y < dpi->top && y2 < dpi->top)
			return;

		t = dpi->left + dpi->width;
		if (x > t && x2 > t)
			return;

		t = dpi->top + dpi->height;
		if (y > t && y2 > t)
			return;
	}

	dy = (y2 - y) * 2;
	if (dy < 0) { dy = -dy;  stepy = -1; } else { stepy = 1; }

	dx = (x2 - x) * 2;
	if (dx < 0) { dx = -dx;  stepx = -1; } else { stepx = 1; }

	GfxSetPixel(x, y, color);
	if (dx > dy) {
		frac = dy - (dx >> 1);
		while (x != x2) {
			if (frac >= 0) {
				y += stepy;
				frac -= dx;
			}
			x += stepx;
			frac += dy;
			GfxSetPixel(x, y, color);
		}
	} else {
		frac = dx - (dy >> 1);
		while (y != y2) {
			if (frac >= 0) {
				x += stepx;
				frac -= dy;
			}
			y += stepy;
			frac += dx;
			GfxSetPixel(x, y, color);
		}
	}
}

/* returns right coordinate */
int DrawString(int x, int y, uint16 str, byte color)
{
	GetString(str_buffr, str);
	return DoDrawString(str_buffr, x, y, color);
}

void DrawStringMultiLine(int x, int y, uint16 str, int maxw) {
	uint32 tmp;
	int num, w, mt, t;
	byte *src;
	byte c;

	GetString(str_buffr, str);

	tmp = FormatStringLinebreaks(str_buffr, maxw);
	num = (uint16)tmp;
	t = tmp >> 16;
	mt = 10;
	if (t != 0) {
		mt = 6;
		if (t != 244) mt = 18;
	}

	src = str_buffr;

	for(;;) {
		w = GetStringWidth(src);
		DoDrawString(src, x, y, 0xFE);
		_stringwidth_base = _stringwidth_out;

		for(;;) {
			c = *src++;
			if (c == 0) {
				y += mt;
				if (--num < 0) {
					_stringwidth_base = 0;
					return;
				}
				break;
			} else if (c >= ' ') {
			} else if (c <= '\xA') {
				src++;
			} else if (c <= '\xF') {
			} else { 
				src += 2;
			}
		}
	}
}

int GetStringWidth(const byte *str) {
	int w = -1;
	byte c;
	int base = _stringwidth_base;

	for(;;) {
		c = *str++;
		if (c == 0)
			return w;
		if (c >= ' ') {
			w += _stringwidth_table[base + ((byte)c) - 0x20];			
		} else {
			if (c <= '\xA') str++;
			else if (c == '\xE') base = 224;
			else if (c == '\xF') base = 448;
			else if (c > '\xF') str += 2;
		}
	}
}

void DrawFrameRect(int left, int top, int right, int bottom, int ctab, int flags) {
	byte color_2 = _color_list[ctab].window_color_1a;
	byte color_interior = _color_list[ctab].window_color_bga;
	byte color_3 = _color_list[ctab].window_color_bgb;
	byte color = _color_list[ctab].window_color_2;

	if (!(flags & 0x8))	{
		if (!(flags & 0x20)) {
			GfxFillRect(left, top, left, bottom-1, color);
			GfxFillRect(left+1, top, right-1, top, color);
			GfxFillRect(right, top, right, bottom-1, color_2);
			GfxFillRect(left, bottom, right, bottom, color_2);
			if (!(flags & 0x10)) {
				GfxFillRect(left+1, top+1, right-1, bottom-1, color_interior);	
			}
		} else {
			GfxFillRect(left, top, left, bottom, color_2);
			GfxFillRect(left+1, top, right, top, color_2);
			GfxFillRect(right, top+1, right, bottom-1, color);
			GfxFillRect(left+1, bottom, right, bottom, color);
			if (!(flags & 0x10)) {
				GfxFillRect(left+1, top+1, right-1, bottom-1, 
					flags&0x40 ? color_interior : color_3);
			}
		}
	} else {
		GfxFillRect(left, top, right, bottom, color_interior);		
	}
}

int DoDrawString(const byte *string, int x, int y, byte color) {
	DrawPixelInfo *dpi = _cur_dpi;
	int base = _stringwidth_base;
	byte c, *b;
	int xo = x, yo = y;

	if (color != 0xFE) {
		if (x >= dpi->left + dpi->width ||
				x + _screen.width*2 <= dpi->left ||
				y >= dpi->top + dpi->height ||
				y + _screen.height <= dpi->top)
					return x;
		
		if (color != 0xFF) {
switch_color:;			
			b = GetSpritePtr(674);
			_string_colorremap[1] = b[color*2+8];
			_string_colorremap[2] = b[color*2+9];
			_color_remap_ptr = _string_colorremap;
		}	
	}

	if (y + 19 <= dpi->top || dpi->top + dpi->height <= y) {
skip_char:;
		for(;;) {
			c = *string++;
			if (c < 32) goto skip_cont;
			if (c >= 0x88 && c <= 0x98) goto color_char;
		}
	}

	for(;;) {
		c = *string++;
skip_cont:;
		if (c == 0) {
			_stringwidth_out = base;
			return x;
		}
		if (c >= 0x88 && c <= 0x98) { // Is inside set color range?
color_char:;
			color = (byte)(c - 136);
			goto switch_color;			
		}
		if (c >= 32) {
			if (x >= dpi->left + dpi->width) goto skip_char;
			if (x + 26 >= dpi->left) {
				GfxMainBlitter(GetSpritePtr(base + 2 + c - ' '), x, y, 1);	
			}
			x += _stringwidth_table[base + c - ' '];
		} else if (c == 0xD) { // newline = {}
			x = xo;
			y += 10;
			if (base != 0) {
				y -= 4;
				if (base != 0xE0)
					y += 12;
			}
		} else if (c == 0x1) { // {SETX}
			x = xo + *string++;
		} else if (c == 0x1F) {// {SETXY}
			x = xo + *string++;
			y = yo + *string++;
		} else if (c == 0xE) { // {TINYFONT}
			base = 0xE0;
		} else if (c == 0xF) { // {BIGFONT}
			base = 0x1C0;
		} else {
			printf("Unknown string command character %d\n", c);
		}
	}
}

void DrawSprite(uint32 img, int x, int y) {
	if (img & 0x8000) {
		_color_remap_ptr = GetSpritePtr(img >> 16) + 1;
		GfxMainBlitter(GetSpritePtr(img & 0x3FFF), x, y, 1);
	} else if (img & 0x4000) {
		_color_remap_ptr = GetSpritePtr(img >> 16) + 1;
		GfxMainBlitter(GetSpritePtr(img & 0x3FFF), x, y, 2);
	} else {
		GfxMainBlitter(GetSpritePtr(img & 0x3FFF), x, y, 0);
	}
}

typedef struct BlitterParams {
	int start_x, start_y;
	byte *sprite, *sprite_org;
	byte *dst;
	int mode;
	int width, height;
	int width_org, height_org;
	int pitch;
	byte info;
} BlitterParams;

static void GfxMainBlitter(byte *sprite, int x, int y, int mode)
{
	DrawPixelInfo *dpi = _cur_dpi;
	int start_x, start_y;
	byte info;
	BlitterParams bp;

	/* decode sprite header */
	x += (int16)READ_LE_UINT16(&((SpriteHdr*)sprite)->x_offs);
	y += (int16)READ_LE_UINT16(&((SpriteHdr*)sprite)->y_offs);
	bp.width_org = bp.width = READ_LE_UINT16(&((SpriteHdr*)sprite)->width);
	bp.height_org = bp.height = ((SpriteHdr*)sprite)->height;
	info = ((SpriteHdr*)sprite)->info;
	bp.info = info;
	bp.sprite_org = bp.sprite = sprite + sizeof(SpriteHdr);
	bp.dst = dpi->dst_ptr;
	bp.mode = mode;
	bp.pitch = dpi->pitch;

	assert(bp.height > 0);
	assert(bp.width > 0);
}
