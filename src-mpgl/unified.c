/**
 * MicroProse Graphics Library
 *
 * The MicroProse Graphics Library is intended to be a collection of high quality routines
 * to perform the primitive graphic functions normally required in our games.
 *
 * The routines are combined together to form a consistent interface between the applications.
 *
 * As much as possible, graphics adapter specific logic has been hidden
 * to make the underlying hardware transparent to the user.
 *
 * Each game will of necessity embellish the library with custom features.
 *
 * Hopefully the library will at least provide the required primitives.
 */

/*
///////////////////////////////////////////////////////
//  __      __  ____  _ __  _ __  (_) _ __    __ _   //
//  \ \ /\ / / / _. || '__|| '_ \ | || '_ \  / _` |  //
//   \ V  V / | (_| || |   | | | || || | | || (_| |  //
//    \_/\_/   \__,_||_|   |_| |_||_||_| |_| \__, |  //
//                                           |___/   //
///////////////////////////////////////////////////////
//                                                   //
// The function names and arguments have not been    //
//   verified. A proper analysis of these routines   //
//   should be performed and each definition updated //
//                                                   //
///////////////////////////////////////////////////////
*/

typedef void * PTR;
typedef unsigned short UINT;
typedef signed short INT;

NULL;  // TODO


/**
 * RastPort
 *
 * Many of the C-callable graphics routines get their parameters from a RastPort.
 * A RastPort is a definition of a window into a screen.
 * It defines the boundaries of that window for clipping and relative addressing
 * as well as maintaining current pen colors and font info for text drawing.
 *
 * The file RastPort contains a definition of the structure.
 */

#if AMIGA
# include "RastPort.h"
#endif


/**
 * Map a graphic page to a memory segment.
 */
void ShowPage()
  // TODO: args
{
  // TODO
}


/**
 * Returns library-specific data parameters one at a time.
 * Programs would have an include file with defines for the indexes.
 * Conceptually, this facility should be for static data only.
 */
void DumpParm()
{
  // TODO
}

/**
 * Load in the selected overlay and setup vector table.
 *
 * This is a software only function. (TODO: did this line belong to a different routine?)
 */
void LoadProg()
{
  // TODO
}


/**
 * To load the new palette into the hardware registers, a SetPalette is required.
 */
void SetPalette(palette, index, count)
  PTR palette;
  UINT index;
  UINT count;
{
  // TODO
}


/**
 * Set video mode for this library plus any other library specific initialization
 *
 * The mode argument is the same value used for LibCanDo.
 * And is unused in libraries supporting a single video mode.
 */
void SetMode(mode)
  UINT mode;
{
  // TODO
}


/**
 * Determine what graphics modes are supported in the hardware.
 * \return The code for the highest performance video system found
 * It would be nice if this code was used system-wide.
 */
UINT GetBestMode()
{
  return 13h;  // TODO
}


/// This group of functions operate on entire graphic pages.

/**
 * Display the selected graphics page on the CRT.
 * Otherwise a blast is performed, overwriting the data previously in video memory.
 */
void ShowPageOverwrite(page)
  PTR page;
{
  // TODO
}


/**
 * Display the selected graphics page on the CRT.
 * Otherwise a non-destructive ShowPage is performed, swapping the data with the video buffer.
 */
void ShowPageSwap(page)
  PTR page;
{
  // TODO
}


/**
 * A random dissolve is performed from page to the current video page.
 * This is a destructive copy in all video modes.
 */
void Dissolve(page)
  PTR page;
{
  // TODO
}


/// This group of functions operate on windows of graphic pages.

/**
 * Copy a window from one graphics page to another.
 */
void BitBlt(sx, sy, width, height, dx, dy)
  UINT width;
  UINT height;
{
  // TODO
}


/**
 * Fill a window in a RastPort with a color.
 */
void FillBox(x, y, width, height, color, op)
  UINT width;
  UINT height;
  UINT color;
  UINT op;
{
  // TODO
}


/// These routines read a picture from a disk file into a graphics page.
/// Any conversion or decompression is performed automatically.
/// Currently two file formats are supported: IFF for development and CRUNCHED for production.
/// The picture file contains color palette data that is loaded as well.
/// For color pictures, the palette is 16 color values stored in bytes.
/// With the upper nibble being blank.

INT LoadPicture(filename, page)
  PTR filename;
  PTR page;
{
  // TODO

  return 0;
}


// TODO: others??


/// The following are used internally by LoadPicture.

void LzwDecode()
  // TODO: args
{
  // TODO
}

// TODO: other internal functions


/// The following code illustrates the intended usage.

// TODO


/**
 * Sprites
 *
 * Sprites are defined by a structure and are passed as arguments with pointers to that structure.
 *
 * The header of the structure defines the size of the sprite and other parameter data.
 * Following the header is the compressed picture data.
 * Each scan line is defined by two size bytes and the variable length data.
 * The first size byte gives the relative offset to the first non-blank pixel.
 * The second size byte gives the length of data following.
 *
 * There was a conceptual problem with building sprites offline, but I can't remember what it is.
 *
 * Sprites can be built online or offline.
 * An offline sprite compiler will allow sprites to be extracted from graphic images,
 * formed into structures, and saved in disk files.
 * An online facility will exist to perform the same function at the expense of longer load times.
 * Sprites contain the destination page for writing and display.
 * Sprites ALWAYS exist in real memory and are in pixel format.
 * It is still an open issue as to whether or not a separate mask plane is required.
 */

// TODO
