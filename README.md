# TODO: move wiki from old repo (cae) to current repo, delete old repo

Covert Action Engine
====================

**Next major focus: overlays**

For duplicating the source of the original engine, it would be necessary to get overlays functional using the original compiler. Without this, there is no chance of generating the various executables needed for recreating the original source from scratch.

-----

<mark>TODO</mark>: switch to Allegro 4.2 and eventually remove all library dependencies

<mark>TODO</mark>: split this into two projects: an "original" made for DOS and a "rewrite" made with SDL3 for Linux

I made this repository to practice in several domains:

- C++ standard library and some newer C++ functionality
- Cross-platform GUI, specifically SDL2
- Reverse engineering
- Unit testing
- Debugging
- CMake

Currently, I build this using CMake on Fedora 36 with a few libsdl-org package (SDL2, TTF, Mixer, Image) as well as zlib and libpng. If there are other dependencies, those will either be in the CMakeLists.txt file or can be installed with a package manager or compiled from source.

With significant manual configuration, this will [build on Windows 10 using Visual Studio](BUILD.md).

Documentation of the build process (for any platform) is on the back burner.

Time permitting, I will try to independently develop compatible art to support users who do not have the original data files. This is particularly challenging, as there are case files, password lists, a face generator, and animated graphics in a proprietary format (and some of the strings contained in the program executable, so I would need to choose between attempting to create a 1:1 duplicate of the original source and a rewrite that has defaults that are overwritten in the presence of the original game file). Also, it would probably take quite a bit of reverse engineering to figure out where each resource is located in the original files, such as font face, graphics coordinates for the map display, linkages between partially hidden events, and more. I would also hope to have a method of parsing all of the original data files.

## Doxygen for inheritance diagrams

```sh
doxygen dox.cfg
```

## More Info

[Wiki](https://github.com/PennRobotics/covert-action-engine/wiki) - describes build flags and file organization

[BUILD.md](BUILD.md) - contains commands for compiling from source  
[RANDOM.md](RANDOM.md) - assorted thoughts on content creation to avoid copyright/license violation  
[REFERENCES.md](REFERENCES.md) - links to resources useful during programming  
[TODO.md](TODO.md) - checklist of identified improvement areas  
[TESTTODO.md](./test/TESTTODO.md) - checklist of changes needed in the test suite  
[content/](./content/) - directory with summaries of each data storage method  


<div id="badges">
  <a href="https://www.linkedin.com/in/pennrobotics/"><img src="https://img.shields.io/badge/LinkedIn-steelblue?style=flat&logo=linkedin&logoColor=white" alt="LinkedIn Badge"/></a>
  <a href="https://github.com/PennRobotics/"><img src="https://img.shields.io/badge/Github-darkslategrey?style=flat&logo=github&logoColor=white" alt="GitHub Badge"/></a>
  <a href="https://news.ycombinator.com/user?id=PennRobotics"><img src="https://img.shields.io/badge/HackerNews-orange?style=flat&logo=ycombinator&logoColor=white" alt="Hacker News Badge"/></a>
  <a href="https://stackoverflow.com/users/10641561"><img src="https://img.shields.io/badge/StackOverflow-darkorange?style=flat&logo=stackoverflow&logoColor=white" alt="Stack Overflow Badge"/></a>
</div>
